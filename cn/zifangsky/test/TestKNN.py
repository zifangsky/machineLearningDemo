import numpy
import cn.zifangsky.kNN.kNN as kNN

# 创建数据集和标签
def createTestDataSet():
    group = numpy.array([[1.0,1.1],[1.0,1.0],[0,0],[0,0.1]])
    labels = ['A', 'A', 'B', 'B']

    return group, labels


group,labels = createTestDataSet()

#测试分类算法——classify0
print(kNN.classify0([0,0], group, labels,2))