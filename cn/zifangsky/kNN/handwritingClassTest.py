# 导入科学计算包
import numpy
import os
# 引入前面写的k-邻近算法
import cn.zifangsky.kNN.kNN as kNN

'''
将文本记录转换成NumPy数组
    fileName 文件路径
'''
def img2vector(fileName):
    # 最后返回的数组
    resultVector = numpy.zeros((1, 1024))

    f = open(fileName)
    for i in range(32):
        lineStr = f.readline()
        for j in range(32):
            resultVector[0, 32*i+j] = int(lineStr[j])
    return resultVector


'''
使用k近邻算法识别手写数字
    trainDirName 训练集的文件路径
    testDirName 测试集的文件路径
'''
def handwritingClassTest(trainDirName,testDirName):
    # 正确数字列表
    correctNumberLabels = []
    # 训练集
    trainingFileList = os.listdir(trainDirName)
    # 训练集的文件数
    trainingFileCount = len(trainingFileList)
    # 训练集的Numpy矩阵
    trainingMat = numpy.zeros((trainingFileCount, 1024))

    for i in range(trainingFileCount):
        # 文件名
        fileNameStr = trainingFileList[i]
        correctNumberStr = fileNameStr.split('.')[0].split('_')[0]
        # 正确的数字
        correctNumber = int(correctNumberStr)
        correctNumberLabels.append(correctNumber)

        trainingMat[i,:] = img2vector(trainDirName + '/%s'%fileNameStr)

    # 测试集
    testFileList = os.listdir(testDirName)
    # 错误数
    errorCount = 0.0
    # 测试集的文件数
    testFileCount = len(testFileList)

    for i in range(testFileCount):
        # 文件名
        fileNameStr = testFileList[i]
        correctNumberStr = fileNameStr.split('.')[0].split('_')[0]
        # 正确的数字
        correctNumber = int(correctNumberStr)

        # 待预测数据
        testVector = img2vector(testDirName + '/%s'%fileNameStr)
        # 预测结果
        classifierResult = kNN.classify0(testVector, trainingMat, correctNumberLabels, 3)
        print('预测结果：%d,真实结果：%d'%(classifierResult, correctNumber))

        if(classifierResult != correctNumber):
            errorCount += 1.0

    print('错误总数：%d'%errorCount)
    print('算法的错误率是：%f'%(errorCount / float(testFileCount)))

# 测试代码效果
# testVector = img2vector('C:/Users/zifangsky/Desktop/trainingDigits/0_0.txt')
handwritingClassTest('C:/Users/zifangsky/Desktop/trainingDigits','C:/Users/zifangsky/Desktop/testDigits')