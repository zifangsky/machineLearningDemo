# 导入科学计算包
import numpy
# 导入Matplotlib
import matplotlib.pyplot as plt
import matplotlib.font_manager as font_manager
import cn.zifangsky.kNN.kNN as kNN

# 加载中文字体
font_set = font_manager.FontProperties(fname=r"c:/windows/fonts/simsun.ttc", size=12)

'''
将文本记录转换成NumPy矩阵
    fileName 文件路径
'''
def file2matrix(fileName):
    f = open(fileName)
    lines = f.readlines()
    # 获取文件行数
    linesCount = len(lines)
    # 创建返回的Numpy矩阵
    resultMat = numpy.zeros((linesCount,3))
    # 创建返回的标签列表
    resultLabelVector = []

    index = 0
    for line in lines:
        line = line.strip()
        # 将一行的数据通过 \t 分割成多个元素
        lineArr = line.split('\t')
        # 取前三个元素
        resultMat[index,:] = lineArr[0:3]
        # 最后一个元素作为标签
        resultLabelVector.append(int(lineArr[-1]))
        index += 1

    return resultMat,resultLabelVector


'''
使用Matplotlib创建散点图
    fileName 文件路径
'''
def showInMatplotlib(dataMat,labelVector):
    fig = plt.figure()
    # 一行、一列、当前图型号为1
    ax = fig.add_subplot(111)
    # 显示第二、三项的数据，并利用labelVector的标签标记颜色
    ax.scatter(dataMat[:,0],dataMat[:,1],
               15.0*numpy.array(labelVector), 15.0*numpy.array(labelVector))

    plt.xlabel(u"每年获得的飞行常客里程数", fontproperties=font_set)
    plt.ylabel(u'玩视频游戏所耗时间百分比', fontproperties=font_set)
    plt.title(u'约会数据散点图', fontproperties=font_set)
    plt.show()


'''
归一化特征值，公式：newValue = (oldValue-min)/(max-min)
    dataSet 待处理数据集
'''
def autoNorm(dataSet):
    minValue = dataSet.min(0)
    maxValue = dataSet.max(0)
    # 范围
    ranges = maxValue - minValue
    # 数据集的数量
    num = dataSet.shape[0]
    # 首先初始化处理之后的数据集
    normDataSet = numpy.zeros(num)
    # 每个数据分别减去minValue
    normDataSet = dataSet - numpy.tile(minValue, (num, 1))
    # 然后再除以取值范围
    normDataSet = normDataSet / numpy.tile(ranges, (num, 1))

    return normDataSet, ranges, minValue


'''
约会网站预测算法
'''
def classifyPerson():
    # 是否具有吸引力
    resultList = ['not at all','in small doses', 'in large doses']

    # 玩视频游戏所耗时间百分比
    percentTats = float(input('请输入玩视频游戏所耗时间百分比：\n'))
    # 每年获得的飞行常客里程数
    ffMiles = float(input('请输入每年获得的飞行常客里程数：\n'))
    # 每周消费的冰淇淋公升数
    iceCream = float(input('请输入每周消费的冰淇淋公升数：\n'))

    # 初始化样本
    datingDataMat, datingLabels = file2matrix(
        'C:/Users/zifangsky/Desktop/《机器学习实战》源码/Ch02/datingTestSet2.txt')
    # 归一化特征值
    normDataSet, ranges, minValue = autoNorm(datingDataMat)
    # 处理测试数据
    testDataArr = (numpy.array([ffMiles, percentTats, iceCream]) - minValue) / ranges

    # 预测结果
    classifierResult = kNN.classify0(testDataArr, normDataSet, datingLabels, 3)
    print("You will probably like this person:",resultList[classifierResult - 1])

