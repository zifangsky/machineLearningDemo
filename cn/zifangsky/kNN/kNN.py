# 导入科学计算包
import numpy
# 导入运算符模块
import operator

'''
分类算法：返回目标坐标的所属分类
    targetPoint 目标坐标
    dataSet 数据集
    labels 数据集对应的标签
    kNum 选取kNum个最近的点
'''
def classify0(targetPoint, dataSet, labels, kNum):
        # 数据集的数目
        dataSetSize = dataSet.shape[0]
        # 将目标坐标复制dataSetSize份，并分别与dataSet中的每个点计算横纵坐标距离
        diffMat = numpy.tile(targetPoint, (dataSetSize, 1)) - dataSet
        # 将横纵坐标分别平方
        sqDiffMat = diffMat ** 2
        # 将矩阵向量按行相加（axis＝0表示按列相加，axis＝1表示按行相加）
        sqDistances = sqDiffMat.sum(axis=1)
        # 求得距离
        distances = sqDistances ** 0.5
        # 返回从小到大排序之后的元素所在位置的索引值
        sortedDistIndicies = distances.argsort()

        # 字典<标签,出现次数>
        labelCountMap = {}
        # 选择距离最短的K个点
        for i in range(kNum):
            # 返回元素对应的标签
            currentLabel = labels[sortedDistIndicies[i]]
            # 给标签计数，如果之前不存在则设置默认值0
            labelCountMap[currentLabel] = labelCountMap.get(currentLabel,0) + 1

        # 对字典元素按VALUE降序排序
        sortedLabelCountMap = sorted(labelCountMap.items(), key=operator.itemgetter(1),reverse=True)

        return sortedLabelCountMap[0][0]

